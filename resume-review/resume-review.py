import openai
import settings
import PyPDF2
import docx
import os

openai.api_key = settings.OPENAI_API_KEY
openai.organization = settings.OPENAI_ORG

def query_gen(file):
    file_extension = os.path.splitext(file)[1]
    if file_extension == ".pdf":
        with open(file, "rb") as file:
            reader = PyPDF2.PdfReader(file)
            result = ""
            for page in reader.pages:
                result += page.extract_text()
            return result, file_extension
    elif file_extension == ".docx":
        doc = docx.Document(file)
        result = ""
        for paragraph in doc.paragraphs:
            result += paragraph.text + "\n"
        return result, file_extension
    else:
        return "", ""
    
file = input("Enter the file name:")
query = query_gen(file)
query = query + "\nPlease suggest me some changes in my resume"

# Define the conversation history
conversation = [
        {'role': 'system', 'content': 'You are a helpful assistant.'},
    ]
while (query != "exit"):
    # Add user query to the conversation
    conversation.append({'role': 'user', 'content': query})

    # Make the API request
    response = openai.ChatCompletion.create(
        model="gpt-4",
        messages=conversation
    )

    # Extract the assistant's reply
    reply = response['choices'][0]['message']['content']
    conversation.append({'role': 'assistant', 'content': reply})
    # Print the assistant's reply
    print(reply)

    # print(conversation)

    # Get user input
    query = input("Enter your query: ")


