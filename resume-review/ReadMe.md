# Resume Review

## This is a open.ai based resume review system
- One hav eto provide his resume in the .pdf or .docx format
- The the oprn.ai will suggest changes in the resume
    - The changes can be suggest directly from open.ai
    - Or changes will be suggested on the basis of some sample resume


### PREREQUISITES
* Python 3.x
* pip3
* open.ai

### CLONE
```
git clone https://gitlab.com/rajharsh181/be_gpt_projects.git
```
### RUNNING
```
cd be_gpt_projects
```
```
pip install -r requirements.txt
```
```
cd resume-review
```
```
Create a file named settings.py
or you can simply copy it from the templates/settings.py
```
```
OPENAI_API_KEY = "OPENAI-KEY-HERE"
OPENAI_ORG = "ORG-ID-HERE"
Fill these APIs there
Save the new settings.py file inside resume-review folder
```
```
Use the resume-review.py to get your resume reviewed directly from open.ai
```
```
Use the resume_review_sample.py to get your resume reviewed from open.ai based on sample resume
```
