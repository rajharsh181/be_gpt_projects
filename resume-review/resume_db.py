import openai
import settings
from bs4 import BeautifulSoup
import PyPDF2
import docx
import os
import csv

openai.api_key = settings.OPENAI_API_KEY
openai.organization = settings.OPENAI_ORG

def query_gen(file):
    file_extension = os.path.splitext(file)[1]
    if file_extension == ".pdf":
        with open(file, "rb") as file:
            reader = PyPDF2.PdfReader(file)
            result = ""
            for page in reader.pages:
                result += page.extract_text()
            return result
    elif file_extension == ".docx":
        doc = docx.Document(file)
        result = ""
        for paragraph in doc.paragraphs:
            result += paragraph.text + "\n"
        return result
    else:
        return ""

# file = input("Enter the file name:")
# query = query_gen(file)

# Define the conversation history
conversation = [
        {'role': 'system', 'content': 'You are a helpful assistant.'},
    ]

for i in range (1,5):
    print(i)
    file = "sample_resume/sample" + str(i) + ".pdf"
    query = query_gen(file)
    query = query +"\n Please don't reply on this as this is a sample resume"
    # while (query != "exit"):
    # Add user query to the conversation
    conversation.append({'role': 'user', 'content': query})
    # Make the API request
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=conversation
    )
    # Extract the assistant's reply
    reply = response['choices'][0]['message']['content']
    conversation.append({'role': 'assistant', 'content': reply})
    # Print the assistant's reply
    print(reply)
    print("\n\n")
    # Get user input
    # file = input("Enter the file name:")
    # query = query_gen(file)



# Specify the file path
file_path = 'resume_db.csv'
# Extract field names from the first dictionary
field_names = conversation[0].keys()
# Write data to the CSV file with 'utf-8' encoding
with open(file_path, 'w', newline='', encoding='utf-8') as file:
    writer = csv.DictWriter(file, fieldnames=field_names)
    # Write the header
    writer.writeheader()
    # Write the data rows
    writer.writerows(conversation)
