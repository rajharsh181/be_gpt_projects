import openai
import settings
import PyPDF2
import docx
import os
import csv

openai.api_key = settings.OPENAI_API_KEY
openai.organization = settings.OPENAI_ORG

def query_gen(file):
    file_extension = os.path.splitext(file)[1]
    if file_extension == ".pdf":
        with open(file, "rb") as file:
            reader = PyPDF2.PdfReader(file)
            result = ""
            for page in reader.pages:
                result += page.extract_text()
            return result, file_extension
    elif file_extension == ".docx":
        doc = docx.Document(file)
        result = ""
        for paragraph in doc.paragraphs:
            result += paragraph.text + "\n"
        return result, file_extension
    else:
        return "", ""

def resume_db():
    # Specify the file path
    file_path = 'resume_db.csv'
    # Create an empty list to store the conversations
    conversations = []

    # Read the CSV file
    with open(file_path, 'r', encoding='utf-8') as file:
        reader = csv.DictReader(file)
        for row in reader:
            # Extract the role and content from each row
            role = row['role']
            content = row['content']
            # Create a dictionary for each conversation item
            conversation_item = {'role': role, 'content': content}
            # Add the conversation item to the conversations list
            conversations.append(conversation_item)
    return conversations

file = input("Enter the file name:")
query = query_gen(file)
query = query + "\nPlease suggest me some changes in my resume"

# Using the resume_db function to create the conversation where the sample resumes are stored
conversation = resume_db()

# Initilizing a while loop 
while (query != "exit"):
    # Add user query to the conversation
    conversation.append({'role': 'user', 'content': query})

    # Make the API request
    response = openai.ChatCompletion.create(
        model="gpt-4",
        messages=conversation
    )

    # Extract the assistant's reply
    reply = response['choices'][0]['message']['content']
    conversation.append({'role': 'assistant', 'content': reply})
    # Print the assistant's reply
    print(reply)

    # Get user input for next query
    query = input("Enter your query: ")


